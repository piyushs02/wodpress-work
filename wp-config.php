<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', 'wordpress');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'FLH+)hj~%LSog2@&od?^LeH>Oy@XClt7.y-HeSv+U]eLG?_?DUdxg>,d`!#7&VGn');
define('SECURE_AUTH_KEY',  '#|g6H#{^uK?<=L ~EcKr+3c^aR,hemotshZu C-Sqs^Y4/l]Ts>?MeJF5:c!.U,.');
define('LOGGED_IN_KEY',    'bWc(sy_i3 YKb)Jcft#Br,)!~aM;y,4j9a#&LWhn~N@|EbXU:HN?(jy494~c7xX2');
define('NONCE_KEY',        'P]0Zv`*7:rs-4dPVo8/H~JzTgOwV1^Jo&(x:gR#RD0MaJ!I>n|V#@e6?TMDutUL8');
define('AUTH_SALT',        '3A0chH;{g*ro<C0=]w 9L+>V!Gs8@jM3nsydUh{uBj1?!Xm_$C~3}r]_&!%({8H>');
define('SECURE_AUTH_SALT', 'Vy#1Sh<l00f-ao5)ZyU&$LPKiKy[>*}>3 H10F*)mmgC2CGVi7_l#>-<c7r!-{Y{');
define('LOGGED_IN_SALT',   '*$8|4HI.5cdmZ3g_<e7*DN;c}!m6w+cku!C.2BY,,Aq]f@%6k/~BX[(^s)dcA?uQ');
define('NONCE_SALT',       'eY{VR!(u0J2?lud_%(9m}1`EEW~h>x76jH/+d89- ^op!s<o;E7B:&,sM IJm(x=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
